# Mapas Conceptuales
## Industria 4.0
```plantuml
@startmindmap
title Mapa Conceptual Industria 4.0
* Industria 4.0
** Historia
*** Industria 1.0
**** Año 1784
***** Prouccion Mecanica \nEnergia a Vapor e Hidraulica
*** Industria 2.0
**** Año 1870
***** Linea de Ensamblaje \nEnergia Electrica \nProduccion en masa
*** Industria 3.0
**** Año 1969
***** Produccion Automatizada \nProgrammable Logic Controller PLC
*** Industria 4.0
**** Actualidad
***** Produccion Inteligente
****** Tecnologias
******* Robotica
******* Big Data
******* Cloud Computing
******* Sistema Autonomo
******* IoT
******* Inteligencia Artificial
** Cambios
*** Integracion de TIC \nen la industria de \nManufactura y servicios
**** Efectos
***** Reduccion de puestos \nde trabajo
****** 45 por ciento de \nlos trabajos desapareceran \nen los proximos 25 años 
***** Aparicion de nuevas \nprofesiones
****** Youtubers \nDesarrolladores de Apps \nConsultores de Redes Solciales \nConductores de Ubers \nPilotos de Drones
*** Transformacion de las \nempresas de manufactura \nen empresas de TICs
**** Efectos
***** Menos diferenciacion \nentre las industrias
*** Nuevos paradigmas \ny tecnologias
**** Velocidad
***** En los ultmos 15 años \ndesaparecieron 52 porciento \nde empresas por su lentitud \nal adaptarse
**** Negocios basados \nen plataformas
***** Facebook \nAmazon \nNetflix \nGoogle \nApple \nNvidia
***** Baidu \nAlibaba \nTencent
*** Nuevas Culturas \nDigitales
**** Phono Sapiens
***** Milenials \nGeneracion Z \nNacidos en las \nultimas 2 decadas \nTecnologia Digital \nes parte de su vida
@endmindmap
```

## México y la Industria 4.0
```plantuml
@startmindmap
* Mapa Conceptual México \ny la Industria 4.0
** Mexico en la Industria 4.0 \nesta en su punto \nde inflexion
*** Al estar cerca \nde Estados Unidos \nY tener una relacion Industrial \ncon grandes empresas
**** Hay una gran oportunidad \nsi México sabe aprovecharla
***** Moviendo las tendencias \Nde educacion
** Ejemplos en Mexico \nde Industria 4.0
*** StarApp sin llave
**** Aplicacion Movil que \npermite al usuario controlar \nel acceso a puertas \ndesde el celular
**** Funciona con la coneccion \ndel celular y del dispositivo que inventaron
**** Puedes saber quien entra \nquien sale \na que hora
**** Obtienes: \nSeguridad \nComodidad
*** We are Robots
**** Conecta al medico \ncon el paciente \natravez de una aplicacion
**** Ayuda a la terapia fisica \ncon la realidad virtual
**** Patentaron unos \nexoesqueleto modulares \npara cada parte del cuerpo
** Que se espera de la industria 4.0
*** Recorte de costos
*** Optimización
*** Creacion de nuevas areas de negocio 
** Complicaciones
*** Empresas con \nequipo antiguo
*** Desconocimiento 
*** Miedo
** Mas de un 80 <br> por ciento de empresas <br> grandes lo estan usando
@endmindmap
```